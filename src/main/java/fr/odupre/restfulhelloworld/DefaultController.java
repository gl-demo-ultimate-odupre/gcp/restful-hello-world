package fr.odupre.restfulhelloworld;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DefaultController {

    public DefaultController() {
	}

	@GetMapping("/")
	public ResponseEntity<String> entrypoint() {
		return new ResponseEntity<String>("GET Response", HttpStatus.OK);
	}
}
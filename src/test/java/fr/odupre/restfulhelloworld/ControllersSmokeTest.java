package fr.odupre.restfulhelloworld;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ControllersSmokeTest {
    @Autowired
	private DefaultController defaultController;
    @Autowired
	private GreetingController greetingController;

    @Test
	public void contextLoads() throws Exception {
		assertThat(defaultController).isNotNull();
		assertThat(greetingController).isNotNull();
	}
}
